from django import forms
from .models import Periods


class PeriodosForm(forms.ModelForm):

    class Meta:
        model = Periods
        fields= ['key', 'name', 'date_start', 'date_end']
        widgets = {
            'date_end': forms.DateInput(format='%d/%m/%Y', attrs={'class':'datepicker form-control'}),
            'date_start': forms.DateInput(format='%d/%m/%Y', attrs={'class':'datepicker form-control'}),
            'key': forms.TextInput(attrs={'class': 'form-control'}),
            'name': forms.TextInput(attrs={'class': 'form-control'})
        }

