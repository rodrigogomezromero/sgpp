from django.db import models
from campus.models import Student, Profile, Career,Days
from organizaciones.models import Organization


class Requirements(models.Model):

    title = models.CharField(max_length=100, verbose_name='Título', default='', blank=True)
    description = models.TextField(max_length=500, default='', blank=True, verbose_name='Descripción')
    status = models.BooleanField(default=False, verbose_name='Activo')
    career = models.ForeignKey(Career, blank=True, default="")

    class Meta:
        verbose_name = " Requisito"
        verbose_name_plural = "Requisitos"

    def __str__(self):
        return self.title


class Stays(models.Model):

    KIND_STAY = (
        ('A', 'Área'),
        ('P', 'Proyecto')
    )

    STATUS = (
        ('C', 'cancelada'),
        ('T', 'terminada'),
        ('P', 'proceso'),

    )
    student = models.ForeignKey(Student, verbose_name='Alumno')
    cordinator = models.ForeignKey(Profile, verbose_name='Encargado dentro de la universidad',blank=True,null=True)
    kind_stay = models.CharField(choices=KIND_STAY, max_length=2,verbose_name='Tipo de estancia')
    description = models.CharField(max_length=500, verbose_name='Nombre de proyecto o Área', default='', blank=True)
    status = models.CharField(max_length=2, choices=STATUS, verbose_name='Estatus de la estancia', default='P')
    date_start = models.DateField(verbose_name='Fecha de inicio')
    date_end = models.DateField(verbose_name='Fecha de terminación', blank=True, null=True, )
    reason_cancel = models.TextField(verbose_name='Razón de cancelación', null=True, blank=True)
    requirements = models.ManyToManyField(Requirements,verbose_name='Requitos',through='ManageRequirements')

    # data dias y horario
    shedule_start = models.TimeField(verbose_name='Horario de entrada', blank=True,null=True)
    shedule_end = models.TimeField(verbose_name='Horario de salida', blank=True,null=True)
    days = models.ManyToManyField(Days, verbose_name='Días de asistencia')
    # data encargado

    title_boss = models.CharField(max_length=50, verbose_name='Título', blank=True, help_text='Lic.')
    name_boss = models.CharField(max_length=200, verbose_name='Nombre ',blank=True)
    lastname_boss= models.CharField(max_length=200, verbose_name='Apellidos',blank=True)
    position_boss = models.CharField(max_length=200, verbose_name='Cargo dentro de la organización', blank=True)
    organization_boss = models.ForeignKey(Organization, on_delete=models.SET_NULL,
                                          blank=True, null=True, verbose_name='Organización')
    departament_boss = models.CharField(max_length=200, verbose_name='Área,Departamento o Dirección',
                                        help_text='Dirección de informática',
                                        blank=True)
    email_boss = models.EmailField(verbose_name='Correo Electrónico', blank=True, null=True)
    phone_boss = models.CharField(verbose_name='Teléfono', blank=True, null=True, max_length=20)
    ext_boss = models.CharField(verbose_name='Extensión', blank=True, null=True, max_length=20)

    class Meta:

        verbose_name = " Estancia"
        verbose_name_plural = "Estancias"

    def __str__(self):
        return "{0}".format(self.student)


class ManageRequirements(models.Model):
    stay = models.ForeignKey(Stays, on_delete=models.CASCADE, verbose_name='Estancia')
    requirement = models.ForeignKey(Requirements, on_delete=models.CASCADE, verbose_name='Requisito')
    is_cover = models.BooleanField(default=False, verbose_name='Cubierto')

    class Meta:
        verbose_name = 'Requisito de estancia'
        verbose_name_plural = 'Requisitos de estancia'

    def __str__(self):

        return '({0}) {1}'.format('Cubierto' if self.is_cover else 'Faltante', self.requirement)


class Projects(models.Model):

    name = models.CharField(max_length=400, blank=False, verbose_name='Nombre del programa o proyecto')
    boss = models.CharField(max_length=300, verbose_name='Responsable')
    position = models.CharField(max_length=300, verbose_name='Cargo')
    duration = models.CharField(max_length=300, verbose_name='Duración')
    implementation_field = models.CharField(max_length=400, verbose_name='Campo de aplicacíon',
                                            blank=True, null=True,
                                            help_text='En caso de que aplique')

    objetive = models.TextField(max_length=600, verbose_name='Objetivo o finalidad')
    stay = models.ForeignKey(Stays)
    active = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Proyecto de estancia'
        verbose_name_plural = 'Proyectos de estancia'

    def __str__(self):
        return self.name


class Areas(models.Model):
    name = models.CharField(max_length=200, verbose_name='Área o Departamento ')
    hours = models.PositiveIntegerField(verbose_name='Horas por actividad')
    activities = models.TextField(max_length=400,verbose_name='Actividades')
    stay = models.ForeignKey(Stays)
