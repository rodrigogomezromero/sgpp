
from django import forms
from .models import Organization, TypeOrganization


class TypeOrganizationForm(forms.ModelForm):

    class Meta:
        model = TypeOrganization
        fields = ['name']
        widget = {
            'name': forms.TextInput(attrs={'class': 'form-control col-12'})
        }


class OrganizationForm(forms.ModelForm):

    class Meta:
        model = Organization
        fields = ['name', 'business_name', 'owner', 'phone', 'type_organization', 'address']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'business_name': forms.TextInput(attrs={'class': 'form-control'}),
            'owner': forms.TextInput(attrs={'class': 'form-control'}),
            'phone': forms.TextInput(attrs={'class': 'form-control'}),
            'type_organization': forms.Select(attrs={'class': 'form-control'}),
            'address': forms.Textarea(attrs={'class': 'form-control'}),
        }

