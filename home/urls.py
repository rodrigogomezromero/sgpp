

from django.conf.urls import url
from .views import *

from alumnos.views import change_password

from django.contrib.auth.forms import AdminPasswordChangeForm


urlpatterns = [
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),

    # url de periodos
    url(r'^periodos/$', PeridoListView.as_view(), name='periodos_list'),
    url(r'^periodos/agregar$', PeridoCreateView.as_view(), name='periodos_new'),
    url(r'^periodos/(?P<pk>\d+)/editar/$', PeridoUpdateView.as_view(), name='periodos_update'),
    url(r'^periodos/(?P<pk>\d+)/eliminar/$', PeridoDeleteView.as_view(), name='periodos_delete'),

]

