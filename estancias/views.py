from django.shortcuts import render
from django.views import generic
from django.core.urlresolvers import reverse_lazy
from .models import Stays,Days
from django.contrib.messages.views import SuccessMessageMixin
from .forms import StayForm


class StaysListView(generic.ListView):
    model = Stays
    template_name = 'stays_list.html'
    context_object_name = 'estancias'
    queryset = Stays.objects.all()

    def get_context_data(self, **kwargs):

        context = super().get_context_data(**kwargs)

        if self.request.user.is_coordinator:
            context['estancias'] = Stays.objects.filter(student__career_id=self.request.user.profile.career_id,
                                                        student__campus_id=self.request.user.profile.campus_id)
        elif self.request.user.is_student:
            context['estancias'] = Stays.objects.filter(student=self.request.user.student)
        else:
            context['estancias'] = Stays.objects.all()
        return context


class StayCreateView(SuccessMessageMixin, generic.CreateView):
    model = Stays
    template_name = 'stays_create.html'
    success_url = reverse_lazy('stays_list')
    form_class = StayForm
    success_message = ' Se ha creado la estancia con éxito'

    def get(self, request, *args, **kwargs):
        get = super(StayCreateView, self).get(request,args,kwargs)

        if self.request.user.is_student:
            if self.request.user.student.stays_set.filter(status='P').count() > 1:
                return render(request, template_name="405.html", context={'message':'No puedes registrar una estancia si ya tienes una en proceso'},status=405)
        else:
            return get

    def get_context_data(self, **kwargs):

        context = super(StayCreateView, self).get_context_data(**kwargs)

        if self.request.user.is_student:
            context['student'] = self.request.user.id
            context['coordinator'] = self.request.user.student.get_cordinator().pk
            context['days'] = Days.objects.all()

        return context


class StayUpdate(SuccessMessageMixin, generic.UpdateView):
    model = Stays
    template_name = 'stays_create.html'
    success_url = reverse_lazy('stays_list')
    form_class = StayForm
    success_message = ' Se ha actualizado la estancia con éxito'


class StaysDelete(SuccessMessageMixin, generic.DeleteView):
    model = Stays
    template_name = 'stays_delete.html'
    success_url = reverse_lazy('stays_list')
    success_message = ' Se ha eliminado la estancia con éxito'


class RegistryStay(generic.TemplateView):
    template_name = 'documentos/registro_estancia.html'