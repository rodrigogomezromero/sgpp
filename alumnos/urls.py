
from django.conf.urls import url
from .views import *


urlpatterns = [
   
    url(r'^$',ListStudentsView.as_view(), name='students_list'),
    url(r'^agregar$', CreateStudent.as_view(), name='students_new'),
    url(r'^editar/(?P<pk>\d+)/$', UpdateStudentView.as_view(), name='students_update'),
    url(r'^editar/(?P<pk>\d+)/password$', change_password, name='students_update_password'),
    url(r'^eliminar/(?P<pk>\d+)/$', DeleteStudentView.as_view(), name='students_delete'),


]
