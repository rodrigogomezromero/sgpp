from django.contrib import admin

from .models import *

admin.site.register(Stays)
admin.site.register(Requirements)
