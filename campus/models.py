
from django.db import models

from django.db import models
from django.contrib.auth.models import User
from django.contrib.auth.models import AbstractUser


class Days(models.Model):
    day = models.PositiveIntegerField(verbose_name='Número día de la semana')
    day_name = models.CharField(verbose_name='Nombre día de la semana', max_length=30)

    class Meta:
        verbose_name_plural= 'Días'
        verbose_name = 'Día'

    def __str__(self):
        return self.day_name


class Periods(models.Model):
    key = models.CharField(max_length=20, verbose_name='Periodo escolar', help_text='SE01-2018')
    name = models.CharField(max_length=200, verbose_name='Nombre de periodo', help_text='Primavera 2018')
    date_start= models.DateField(verbose_name='Fecha de inicio')
    date_end = models.DateField(verbose_name='Fecha final')

    class Meta:
        verbose_name_plural= "Periodos"
        verbose_name = "Periodo"

    def __str__(self):
        return "{0}( {1} - {2} )".format(self.key, self.date_start.strftime("%d/%m/%Y"), self.date_end.strftime("%d/%m/%Y"))


class City(models.Model):

    name = models.CharField(max_length=80, verbose_name='Nombre')

    class Meta:
        verbose_name = "Ciudad sede"
        verbose_name_plural = "Ciudades Sede"

    def __str__(self):
        return self.name


class Campus(models.Model):
    name = models.CharField(max_length=200,  verbose_name='Nombre')
    city = models.ForeignKey(City, on_delete=models.CASCADE,  verbose_name='Ciudad')
    acronym = models.CharField(max_length=80, verbose_name='Siglas', unique=True, help_text='CUN')

    class Meta:
        verbose_name = " Unidad Académica"
        verbose_name_plural = "Unidades Académicas "

    def __str__(self):
        return self.name


class Division(models.Model):
    name = models.CharField(max_length=200, verbose_name='Nombre')
    acronym = models.CharField(max_length=80, verbose_name='Siglas', unique=True)
    logo = models.ImageField(verbose_name='Logo', blank=True, null=True)

    class Meta:
        verbose_name = "División"
        verbose_name_plural = "Divisiones"

    def __str__(self):
        return "({0}) {1}".format(self.acronym, self.name)


class Career(models.Model):

    name = models.CharField(max_length=300, verbose_name='Nombre ')
    acronym = models.CharField(max_length=200, unique=True, verbose_name='Siglas', help_text='DCI')
    division = models.ForeignKey(Division, on_delete=models.SET_NULL, blank=True, null=True, verbose_name='División')

    class Meta:
        verbose_name = "Carrera"
        verbose_name_plural = "Carreras"

    def __str__(self):
        return self.name


class Semester(models.Model):

    number = models.PositiveIntegerField(default=1, verbose_name='Semestre en Número', help_text='1',unique=True)
    text = models.CharField(max_length=50, default='primero', verbose_name='Semestre en número ordinal', help_text='Primero', unique=True)

    class Meta:
        verbose_name = 'Semestre'
        verbose_name_plural = 'Semestres'

    def __str__(self):
        return self.text


class User(AbstractUser):
    is_student = models.BooleanField(default=False, verbose_name='es estudiante')
    is_coordinator = models.BooleanField(default=False, verbose_name='Es encargado de carrera')
    is_director_division = models.BooleanField(default=False, verbose_name='Es jefe de division')
    is_admin = models.BooleanField(default=False, verbose_name='Es Administradora')

    def __str__(self):
        return self.get_full_name()


class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    enrolment = models.CharField(max_length=200, unique=True, verbose_name='Matrícula',)
    semester = models.ForeignKey(Semester, verbose_name='Semestre',default="",blank=True)
    career = models.ForeignKey(Career, on_delete=models.CASCADE, verbose_name='Carrera')
    campus = models.ForeignKey(Campus, verbose_name='Unidad Académica' ,default="", blank=True)
    phone = models.CharField(verbose_name='Teléfono', blank=True, null=True, max_length=20)

    valid_credits = models.PositiveIntegerField(default=0, verbose_name='Créditos válidos')

    class Meta:
        verbose_name = "Estudiante"
        verbose_name_plural = "Estudiantes"

    def __str__(self):
        return "({0}) {1}".format(self.enrolment, self.user)

    def get_cordinator(self):
        return Profile.objects.get(career=self.career, active=True)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    title = models.CharField(max_length=10, blank=True, verbose_name='Título', help_text='Lic.')
    career = models.ForeignKey(Career, blank=True, null=True, verbose_name='Carrera del coordinador')
    campus = models.ForeignKey(Campus, blank=True, null=True, verbose_name='Campus donde se encuentra')
    active = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Perfil'
        verbose_name_plural = 'Perfiles'

    def __str__(self):
        return "{0} ({1})".format(self.user, self.career)


class DirectorDivision(models.Model):
    title = models.CharField(max_length=10, blank=True, verbose_name='Título', help_text='Lic.')
    user = models.OneToOneField(User,on_delete=models.CASCADE,primary_key=True)
    division = models.ForeignKey(Division,verbose_name='División')




