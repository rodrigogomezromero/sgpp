var gulp = require('gulp');

var src_scripts = [
    './node_modules/jquery-timepicker/jquery.timepicker.js',
    './node_modules/chart.js/dist/Chart.min.js',
    './node_modules/chart.js/dist/Chart.bundle.min.js'
];
var src_css = ['./node_modules/jquery-timepicker/jquery.timepicker.css'];

gulp.task('default',['css','scripts'], function ()
{


});



gulp.task('scripts',function () {
     return gulp.src(src_scripts)
    .pipe(gulp.dest('./home/static/js/'));
});


gulp.task('css', function ()
{

   return  gulp.src(src_css)
        .pipe(gulp.dest('./home/static/css/'));


});