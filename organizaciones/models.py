from django.db import models


class TypeOrganization(models.Model):
    name = models.CharField(max_length=200, verbose_name='Tipo de Organizacón')

    class Meta:
        verbose_name = "Tipo Organización"
        verbose_name_plural = "Tipos Organización"

    def __str__(self):
        return self.name


class Organization(models.Model):
    name = models.CharField(max_length=200, verbose_name='Nombre de la organización', unique=True)
    business_name = models.CharField(max_length=200, verbose_name='Razón social', unique=True)
    owner = models.CharField(max_length=400, verbose_name='Titular')
    phone = models.CharField(max_length=30, verbose_name='Teléfono', unique=True)
    type_organization = models.ForeignKey(TypeOrganization, on_delete=models.SET_NULL,
                                          blank=True, null=True, verbose_name='Giro')
    address = models.TextField(verbose_name='Dirección empresa', max_length=300, blank=True, null=True)

    class Meta:
        verbose_name = " Organización"
        verbose_name_plural = "Organizaciones"

    def __str__(self):
        return self.name


