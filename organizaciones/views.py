from django.shortcuts import render
from django.views import generic
from .forms import *
from django.core.urlresolvers import reverse_lazy
from .models import *
from estancias.models import Stays

""" CRUD ORGANIZACIONES """


class OrganizationListView(generic.ListView):
    model = Organization
    context_object_name = 'organizations'
    template_name = 'organizations/organizations_list.html'


class OrganizacionUpdateView(generic.UpdateView):

    model = Organization
    success_url = reverse_lazy('organizations_list')
    form_class = OrganizationForm
    template_name = 'organizations/organizations_new.html'


class OrganizationCreateView(generic.CreateView):
    model = Organization
    success_url = reverse_lazy('organizations_list')
    form_class = OrganizationForm
    template_name = 'organizations/organizations_new.html'


class OrganizationDetail(generic.DetailView):
    model = Organization
    context_object_name = 'organization'
    template_name = 'organizations/organizations_detail.html'

    def get_context_data(self, **kwargs):

        data = super().get_context_data(**kwargs)
        data['stays'] = Stays.objects.filter(organization_boss=self.kwargs['pk'])
        return data


""" CRUD TIPO DE ORGANIZACIONES """


class TypoOrganizationList(generic.ListView):
    model = TypeOrganization
    context_object_name = 'tipos'
    success_url = reverse_lazy('type_organization_list')
    template_name = 'type_organization/type_organizations_list.html'


class TypeOrganizationCreateView(generic.CreateView):

    model = TypeOrganization
    success_url = reverse_lazy('type_organization_list')
    form_class = TypeOrganizationForm
    template_name = 'type_organization/type_organization_new.html'


class TypeOrganizationUpdateView(generic.UpdateView):

    model = TypeOrganization
    success_url = reverse_lazy('type_organization_list')
    form_class = TypeOrganizationForm
    template_name = 'type_organization/type_organization_new.html'


