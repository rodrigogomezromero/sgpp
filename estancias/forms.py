from django import forms
from .models import Stays, Areas, Projects,Days
from django.conf import settings
from campus.models import Profile


class StayForm(forms.ModelForm):

    days = forms.ModelMultipleChoiceField(label='Días de trabajo',queryset=Days.objects.all(), widget=forms.CheckboxSelectMultiple(attrs={'class': 'form-control'}))

    class Meta:
        model = Stays
        fields = ['student', 'cordinator', 'kind_stay', 'description', 'status', 'date_start', 'date_end',
                  'title_boss', 'name_boss', 'lastname_boss', 'position_boss', 'organization_boss', 'departament_boss',
                  'email_boss', 'phone_boss', 'ext_boss','shedule_start','shedule_end','days']
        widgets = {


            'kind_stay': forms.Select(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control'}),
            'date_start': forms.DateInput(format="%d/%m%/%Y", attrs={'class': 'form-control datepicker'}),
            'date_end': forms.DateInput(format="%d/%m%/%Y", attrs={'class': 'form-control datepicker'}),
            'shedule_start': forms.TimeInput(attrs={'class': 'form-control timepicker'}),
            'shedule_end': forms.TimeInput(attrs={'class': 'form-control timepicker'}),

            'reason_cancel': forms.Textarea(attrs={'class': 'form-control'}),
            # data cordinator

            'title_boss': forms.TextInput(attrs={'class': 'form-control'}),
            'name_boss': forms.TextInput(attrs={'class': 'form-control'}),
            'lastname_boss': forms.TextInput(attrs={'class': 'form-control'}),
            'position_boss': forms.TextInput(attrs={'class': 'form-control'}),
            'organization_boss': forms.Select(attrs={'class': 'form-control'}),
            'departament_boss': forms.TextInput(attrs={'class': 'form-control'}),
            'email_boss': forms.EmailInput(attrs={'class': 'form-control'}),
            'phone_boss': forms.TextInput(attrs={'class': 'form-control'}),
            'ext_boss': forms.TextInput(attrs={'class': 'form-control'}),



        }


