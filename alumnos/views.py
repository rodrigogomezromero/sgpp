from django.shortcuts import render
from campus.models import Student,User
from django.views import generic
from .forms import StudentForm, StudentCreateForm
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.forms import AdminPasswordChangeForm
from django.contrib.auth import update_session_auth_hash
from django.shortcuts import render, redirect
from django.contrib import messages


def change_password(request,pk):
    if request.method == 'POST':
        user = User.objects.get(id=pk)
        form = AdminPasswordChangeForm(user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'La contraseña del usuario {0} ha sido actualizada cón éxito'.format(user))
            return redirect('students_list')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = AdminPasswordChangeForm(request.user)
    return render(request, 'change_password.html',{
        'form': form
    })


class ListStudentsView(generic.ListView):

    template_name = 'alumnos_list.html'
    model = Student
    paginate_by = 15
    queryset = Student.objects.all()
    context_object_name = 'alumnos'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.request.user.is_superuser:
            context['alumnos'] = Student.objects.all().order_by('enrolment')
        else:
            context['alumnos'] = Student.objects.filter(career_id=self.request.user.profile.career_id,
                                                        campus_id=self.request.user.profile.campus_id)

        return context



class CreateStudent (generic.CreateView):
    model = Student
    form_class = StudentCreateForm
    success_url = reverse_lazy('students_list')
    template_name = 'alumnos_new.html'


class UpdateStudentView(generic.UpdateView):
    model = Student
    form_class = StudentForm
    success_url = reverse_lazy('students_list')
    template_name = 'alumnos_edit.html'


class DeleteStudentView(generic.DeleteView):
    model = Student
    success_url = reverse_lazy('students_list')
    template_name = 'alumnos_delete.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        if self.request.user.is_superuser:
            context['allowed'] = True
        elif self.object.campus_id == self.request.user.profile.campus_id and \
                self.object.career_id == self.request.user.profile.career_id:
            context['allowed'] = True
        else:
            context['allowed'] = False

        return context
