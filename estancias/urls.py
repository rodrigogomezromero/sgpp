from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^$', StaysListView.as_view(), name='stays_list'),
    url(r'^registro_estancia$', RegistryStay.as_view(), name='stays_registro'),

    url(r'^agregar/$', StayCreateView.as_view(), name='stays_new'),
    url(r'^editar/(?P<pk>\d+)/$', StayUpdate.as_view(), name='stays_update'),
]


