from django import forms
from campus.models import *
from django.contrib.auth.forms import UserCreationForm,AdminPasswordChangeForm
from django.db import transaction
from django.contrib.auth.models import Group
from django.http import request


class StudentCreateForm(UserCreationForm):

    def __init__(self, *args, **kwargs):
        super(StudentCreateForm, self).__init__(*args, **kwargs)

    name = forms.CharField(label='Nombre(s)', widget=forms.TextInput(attrs={'class': 'form-control'}))
    lastname = forms.CharField(label='Apellidos', widget=forms.TextInput(attrs={'class': 'form-control'}))
    enrolment = forms.CharField(label='Matrícula', widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(label='Correo electrónico', widget=forms.TextInput(attrs={'class': 'form-control'}))
    semester = forms.ModelChoiceField(required=True,
                                      label='Semestre',
                                      widget=forms.Select(attrs={'class': 'form-control'}),
                                      queryset=Semester.objects.all(),
                                      )

    career = forms.ModelChoiceField(required=True,
                                    widget=forms.Select(attrs={'class': 'form-control'}),
                                    queryset=Career.objects.all(),
                                    label='Licenciatura',
                                    )

    campus = forms.ModelChoiceField(required=True,
                                    widget=forms.Select(attrs={'class': 'form-control'}),
                                    queryset=Campus.objects.all(),
                                    label='Campus'
                                    )
    phone = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control', 'type':'tel'}),label='Teléfono')
    valid_credits = forms.CharField(label='Créditos Válidos', widget=forms.TextInput(attrs={'class': 'form-control'}))

    class Meta(UserCreationForm.Meta):
        model = User



    @transaction.atomic
    def save(self):

        try:
            students_group = Group.objects.get(name='students')
        except Group.DoesNotExist:
            students_group = Group.objects.create(name='students')

        user = super().save(commit=False)
        user.first_name = self.cleaned_data.get('name')
        user.last_name = self.cleaned_data.get('lastname')
        user.email = self.cleaned_data.get('email')
        user.is_student = True

        user.save()
        students_group.user_set.add(user)
        student = Student()
        student.user = user
        student.enrolment = self.cleaned_data.get('enrolment')
        student.semester = self.cleaned_data.get('semester')
        student.career = self.cleaned_data.get('career')
        student.campus = self.cleaned_data.get('campus')
        student.phone = self.cleaned_data.get('phone')
        student.valid_credits = self.cleaned_data.get('valid_credits')
        student.save()
        return user


class StudentForm(forms.ModelForm):

    class Meta:
        model = Student
        fields = ['enrolment', 'semester', 'campus', 'career', 'phone','valid_credits']
        widgets = {

            'enrolment': forms.TextInput(attrs={'class': 'form-control'}),
            'semenster': forms.Select(attrs={'class': 'form-control'}),
            'campus': forms.Select(attrs={'class': 'form-control'}),
            'career':forms.TextInput(attrs={'class': 'form-control'}),
            'valid_credits': forms.NumberInput(attrs={'class': 'form-control'}),
            'phone': forms.TextInput(attrs={'class': 'form-control', 'type': 'tel'}),

        }


