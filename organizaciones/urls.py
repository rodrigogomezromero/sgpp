from django.conf.urls import url
from .views import *

urlpatterns = [
    # Rutas Organizaciones
    url(r'^$',OrganizationListView.as_view(), name='organizations_list'),
    url(r'^agregar/$',OrganizationCreateView.as_view(), name='organizations_new'),
    url(r'^editar/(?P<pk>\d+)/$', OrganizacionUpdateView.as_view(), name='organizations_update'),
    url(r'^(?P<pk>\d+)/$', OrganizationDetail.as_view(), name='organizations_detail'),

    # Rutas tipo organizacion
    url(r'^tipo-organizacion/agregar/$', TypeOrganizationCreateView.as_view(), name='type_organization_new'),
    url(r'^tipo-organizacion/editar/(?P<pk>\d+)/$', TypeOrganizationUpdateView.as_view(), name='type_organization_update'),
    url(r'^tipo-organizacion/$', TypoOrganizationList.as_view(), name='type_organization_list'),


]
