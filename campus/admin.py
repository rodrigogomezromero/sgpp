from django.contrib import admin
from .models import *
from django.contrib.auth.admin import UserAdmin,UserChangeForm,UserCreationForm,AdminPasswordChangeForm


class Useradmin(UserAdmin):
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff','is_superuser', 'is_student', 'is_coordinator')
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),

        (('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser','is_student', 'is_coordinator',
                                     'groups', 'user_permissions')}),
        (('Fechas importantes'), {'fields': ('last_login', 'date_joined')}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'password1', 'password2'),
        }),
    )

    form = UserChangeForm
    add_form = UserCreationForm
    change_password_form = AdminPasswordChangeForm
    pass


class UsuarioAdmin(Useradmin):
    pass


admin.site.register(Periods)
admin.site.register(Division)
admin.site.register(Campus)
admin.site.register(Career)
admin.site.register(Days)

admin.site.register(Student)
admin.site.register(City)
admin.site.register(Profile)
admin.site.register(User, UsuarioAdmin)
admin.site.register(Semester)

